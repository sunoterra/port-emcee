.PHONY: itests tests install all

PLUGIN ?= port-emcee
PYENV ?= . .pyenv/PLUGIN/activate &&
TESTS ?= tests
PREFIX ?= ~/.config/zsh/${PLUGIN}
SHELL ?= zsh
PROJECT ?= $$PWD
BIN ?= ${PROJECT}/bin
CRAM_OPTS ?= '-v'

define ised
  sed $(1) $(2) > "$(2).1"
  mv "$(2).1" "$(2)"
endef

build:
	cat ${PROJECT}/src/${PLUGIN}.zsh > ${BIN}/${PLUGIN}.plugin.zsh
	cat ${PROJECT}/src/constants/*.zsh >> ${BIN}/${PLUGIN}.plugin.zsh
	cat ${PROJECT}/src/common/*.zsh >> ${BIN}/${PLUGIN}.plugin.zsh
	cat ${PROJECT}/src/commands/*.zsh >> ${BIN}/${PLUGIN}.plugin.zsh
	cat ${PROJECT}/src/_${PLUGIN} >> ${BIN}/${PLUGIN}.plugin.zsh
	$(call ised,"s/{{port-emcee_VERSION}}/$$(cat ${PROJECT}/VERSION)/",${BIN}/${PLUGIN}.plugin.zsh)
release: build
	vi VERSION
	git checkout develop
	git checkout -b release/$$(cat ${PROJECT}/VERSION)
	make build && make tests PYENV= SHELL=zsh
	vim README.md
	vim CHANGELOG.md
publish:
	git add .
	git commit --message "build release $$(cat ${PROJECT}/VERSION)" --verbose
	git push --verbose origin release/$$(cat ${PROJECT}/VERSION)
clean:
	rm -f ${PREFIX}/share/${PLUGIN}.plugin.zsh
itests:
	${MAKE} tests CRAM_OPTS=-i
tests:
	${PYENV} ZDOTDIR="${PROJECT}/tests" cram ${CRAM_OPTS} --shell=${SHELL} ${TESTS}
install:
	mkdir -p ${PREFIX}/share && cp ${BIN}/${PLUGIN}.plugin.zsh ${PREFIX}/share/${PLUGIN}.plugin.zsh
deps:
	pip install cram==0.7.*
stats:
	${PROJECT}/tests/stats.sh "${PROJECT}" "${SHELL}"
all: clean build install
