####
#### file:        port-emcee.zsh
#### description: zsh plugin - port-emcee
####              one-word commands for various macports operations:
####              port-udpate, port-upgrade, port-outdated, port-trim
#### references:
####
#### author:      Michael Stilson <michael.stilson@me.com>
#### version:     v0.2.13
####

##
## port-emcee-messages-arguments
## provide invalid/missing argument constants in a form that does not dirty up the environment
##
## arguments are positive integers 0..n-1, valid arguments are:
## 0 - invalid/missing port-name
##
function port-emcee-messages-arguments() {
  local message=""
  case ${1}
  in
    0)
      message="Please specify a valid port name"
    ;;
  esac
  echo "${message}"
}
##
## port-emcee-messages-dependencies
## provide missing dependency constants in a form that does not dirty up the environment
##
## arguments are positive integers 0..n-1, valid arguments are:
## 0 - missing macports
## 1 - missing (g)awk
## 2 - missing grep
## 3 - missing (g)sort
## 4 - missing (g)stat
## 5 - missing port_cutleaves
## 6 - missing sudo
function port-emcee-messages-dependencies() {
  local message=""
  case ${1}
  in
    0)
      message="Please install macports <https://www.macports.org/>"
    ;;
    1)
      message="Please install gawk e.g. \"$ port-install gawk\""
    ;;
    2)
      message="Please install grep e.g. \"$ port-install grep\""
    ;;
    3)
      message="Please install gsort e.g. \"$ port-install coreutils\""
    ;;
    4)
      message="Please install gstat e.g. \"$ port-install coreutils\""
    ;;
    5)
      message="Please install port_cutleaves e.g. \"$ port-install port_cutleaves\""
    ;;
    6)
      message="macports requires sudo privileges"
    ;;
  esac
  echo "${message}"
}
##
## port-emcee-messages-info
## provide info constants in a form that does not dirty up the environment
##
## arguments are positive integers 0..n-1, valid arguments are:
## 0 - install/uninstall header
function port-emcee-messages-info() {
  local message=""
  case ${1}
  in
    0)
      message=" port                     @version[+variant1..+variantN] \
      \n--------------------------------------------------------------------------------"
    ;;
  esac
  echo "${message}"
}
##
## port-emcee-assert-cache
## cache assertions common among all emcee commands
##
function port-emcee-assert-cache() {
  local day; day=86400
  local diff; diff=
  local now; now=$(date "+%s")
  if [[ -z "${PORT_EMCEE_INSTALL}" ]]
  then
    PORT_EMCEE_INSTALL=""
    export PORT_EMCEE_INSTALL
  fi
  if [[ -z "${PORT_EMCEE_UNINSTALL}" ]]
  then
    PORT_EMCEE_UNINSTALL=""
    export PORT_EMCEE_UNINSTALL
  fi
  if [[ -z "${PORT_EMCEE_UPDATES}" ]]
  then
    PORT_EMCEE_UPDATES=""
    export PORT_EMCEE_UPDATES
  fi
  if [[ -z "${PORT_EMCEE_UPDATE_COMPLETE}" ]]
  then
    PORT_EMCEE_UPDATE_COMPLETE=0
    export PORT_EMCEE_UPDATE_COMPLETE
  fi
  if [[ -z "${PORT_EMCEE_UPDATE_TIME}" ]]
  then
    PORT_EMCEE_UPDATE_TIME=${now}
    export PORT_EMCEE_UPDATE_TIME
  fi
  if [[ ${PORT_EMCEE_UPDATE_TIME} -ne ${now} ]]
  then
    diff=$(((now - PORT_EMCEE_UPDATE_TIME)))
    if [[ ${diff} -gt ${day} ]]
    then
      PORT_EMCEE_INSTALL=""
      PORT_EMCEE_UNINSTALL=""
      PORT_EMCEE_UPDATES=""
      PORT_EMCEE_UPDATE_COMPLETE=0
    fi
  fi
}
##
## port-emcee-assert-dependencies
## external binary dependency assertions common among all emcee commands
##
function port-emcee-assert-dependencies() {
  local -a deps; deps=()
  local admin; admin=$(whence -p sudo)
  local gawk; gawk="/opt/local/bin/gawk"
  local grep; grep="/opt/local/bin/ggrep"
  local gsort; gsort="/opt/local/bin/gsort"
  local gstat; gstat="/opt/local/bin/gstat"
  local macports; macports="/opt/local/bin/port"
  local trimmer; trimmer="/opt/local/bin/port_cutleaves"
  while [ "${#}" -gt 0 ]
  do
    case "${1}"
    in
      "gawk")
        if [ ! -x "${gawk}" ]
        then
          gawk="$(whence -p awk)"
        fi
        if [ ! -x "${gawk}" ]
        then
          port-emcee-messages-dependencies 1
          exit 1
        fi
        deps+=(${gawk})
      ;;
      "grep")
        if [ ! -x "${grep}" ]
        then
          grep="$(whence -p grep)"
          exit 1
        fi
        if [ ! -x "${grep}" ]
        then
          port-emcee-messages-dependencies 2
          exit 1
        fi
        deps+=(${grep})
      ;;
      "gsort")
        if [ ! -x "${gsort}" ]
        then
          gsort="$(whence -p sort)"
        fi
        if [ ! -x "${gsort}" ]
        then
          port-emcee-messages-dependencies 3
          exit 1
        fi
        deps+=(${gsort})
      ;;
      "gstat")
        if [ ! -x "${gstat}" ]
        then
          gstat="$(whence -p stat)"
          exit 1
        fi
        if [ ! -x "${gstat}" ]
        then
          port-emcee-messages-dependencies 4
          exit 1
        fi
        deps+=(${gstat})
      ;;
      "port")
        if [ ! -x "${macports}" ]
        then
          port-emcee-messages-dependencies 0
          exit 1
        fi
        deps+=(${macports})
      ;;
      "port_cutleaves")
        if [ ! -x "${trimmer}" ]
        then
          port-emcee-messages-dependencies 5
          exit 1
        fi
        deps+=(${trimmer})
      ;;
      "sudo")
        if [ ! -x "${admin}" ]
        then
          port-emcee-messages-dependencies 6
          exit 1
        fi
        deps+=(${admin})
      ;;
    esac
    shift
  done
  echo "${deps[*]}"
}
##
## port-depof: customize port echo:dependentof PORTNAME
##
function port-depof() {
  local -a deps; deps=($(port-emcee-assert-dependencies "port"))
  local macports; macports="${deps[1]}"
  local package; package=${1}

  eval "${macports} echo dependentof:${package}"
}
##
## port-info: customize port info output
##
function port-info() {
  local -a deps; deps=($(port-emcee-assert-dependencies "port"))
  local macports; macports="${deps[1]}"
  local opts; opts="--fullname --variants --depends --conflicts --category --license"
  local package; package=${1}

  eval "${macports} info ${opts} ${package}"
}
##
## port-install: install ports with default settings; accepts variant arguments
##
function port-install() {
  local -a deps; deps=($(port-emcee-assert-dependencies "port" "sudo"))
  local admin; admin="${deps[2]}"
  local macports; macports="${deps[1]}"
  local -a variants; variants=()
  local package; package=${1}
  local variant; variant="^ *[\+\-].*$"
  # if more than one argument, check for variants
  if [ ${#} -gt 1 ]
  then
    while [ ${#} -gt 0 ]
    do
      if [[ "${1}" =~ ${variant} ]]
      then
        variants+=("${1}")
      fi
      shift
    done
  fi
  if [ -z "${package}" ]
  then
    port-emcee-messages-arguments 0
  else
    # -N non-interactive
    # -c clean after install
    # -f force mode (ignore state files)
    # -s source-only mode
    # -v verbose mode
    eval "${admin} ${macports} -Ncfsv install ${package} ${variants[*]}"
  fi
}
##
## port-outdated: list ports that have updates
##
function port-outdated() {
  local -a deps; deps=($(port-emcee-assert-dependencies "port"))
  local macports; macports="${deps[1]}"

  eval "${macports} -v outdated"
}
##
## port-trim: remove port leaves
##
function port-trim() {
  local -a deps; deps=($(port-emcee-assert-dependencies "port_cutleaves" "sudo"))
  local admin="${deps[2]}" trimmer="${deps[1]}"
  # -b treat build-time required ports like they are not leaves
  eval "${admin} ${trimmer} -b"
}
##
## port-update: perform selfupdate/sync and present a prettified status
##
function port-update() {
  local -a deps; deps=($(port-emcee-assert-dependencies "port" "sudo"))
  local admin; admin="${deps[2]}"
  local macports; macports="${deps[1]}"
  ## selfupdate / sync
  # -v verbose mode
  eval "${admin} ${macports} -v selfupdate"
  ## check for outdated
  port-outdated
}
##
## port-upgrade: upgrade ports that have updates
##
## depends:  port  - a BSD style package management system for mac os.
##
function port-upgrade() {
  local -a deps; deps=($(port-emcee-assert-dependencies "port" "sudo"))
  local admin; admin="${deps[2]}"
  local macports; macports="${deps[1]}"
  # -N non-interactive
  # -p proceed; despite errors
  # -s source-only mode
  # -u uninstall non-active ports
  # -v verbose mode
  local rc
  eval "${admin} ${macports} -Npsuv upgrade outdated"
  rc=${?}
  if [ ${rc} -eq 0 ]
  then
    PORT_EMCEE_INSTALL=""
    export PORT_EMCEE_INSTALL
    PORT_EMCEE_UNINSTALL=""
    export PORT_EMCEE_UNINSTALL
    PORT_EMCEE_UPDATES=""
    export PORT_EMCEE_UPDATES
    echo "cache cleared"
    return ${rc}
  fi
  return ${rc}
}
# compdef _port-emcee
# setup autocompletion
