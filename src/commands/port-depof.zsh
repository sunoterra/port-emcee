##
## port-depof: customize port echo:dependentof PORTNAME
##
function port-depof() {
  local -a deps; deps=($(port-emcee-assert-dependencies "port"))
  local macports; macports="${deps[1]}"
  local package; package=${1}

  eval "${macports} echo dependentof:${package}"
}
