##
## port-info: customize port info output
##
function port-info() {
  local -a deps; deps=($(port-emcee-assert-dependencies "port"))
  local macports; macports="${deps[1]}"
  local opts; opts="--fullname --variants --depends --conflicts --category --license"
  local package; package=${1}

  eval "${macports} info ${opts} ${package}"
}
