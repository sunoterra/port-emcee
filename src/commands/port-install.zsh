##
## port-install: install ports with default settings; accepts variant arguments
##
function port-install() {
  local -a deps; deps=($(port-emcee-assert-dependencies "port" "sudo"))
  local admin; admin="${deps[2]}"
  local macports; macports="${deps[1]}"
  local -a variants; variants=()
  local package; package=${1}
  local variant; variant="^ *[\+\-].*$"
  # if more than one argument, check for variants
  if [ ${#} -gt 1 ]
  then
    while [ ${#} -gt 0 ]
    do
      if [[ "${1}" =~ ${variant} ]]
      then
        variants+=("${1}")
      fi
      shift
    done
  fi
  if [ -z "${package}" ]
  then
    port-emcee-messages-arguments 0
  else
    # -N non-interactive
    # -c clean after install
    # -f force mode (ignore state files)
    # -s source-only mode
    # -v verbose mode
    eval "${admin} ${macports} -Ncfsv install ${package} ${variants[*]}"
  fi
}
