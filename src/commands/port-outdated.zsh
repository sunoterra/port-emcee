##
## port-outdated: list ports that have updates
##
function port-outdated() {
  local -a deps; deps=($(port-emcee-assert-dependencies "port"))
  local macports; macports="${deps[1]}"

  eval "${macports} -v outdated"
}
