##
## port-trim: remove port leaves
##
function port-trim() {
  local -a deps; deps=($(port-emcee-assert-dependencies "port_cutleaves" "sudo"))
  local admin="${deps[2]}" trimmer="${deps[1]}"
  # -b treat build-time required ports like they are not leaves
  eval "${admin} ${trimmer} -b"
}
