##
## port-update: perform selfupdate/sync and present a prettified status
##
function port-update() {
  local -a deps; deps=($(port-emcee-assert-dependencies "port" "sudo"))
  local admin; admin="${deps[2]}"
  local macports; macports="${deps[1]}"
  ## selfupdate / sync
  # -v verbose mode
  eval "${admin} ${macports} -v selfupdate"
  ## check for outdated
  port-outdated
}
