##
## port-upgrade: upgrade ports that have updates
##
## depends:  port  - a BSD style package management system for mac os.
##
function port-upgrade() {
  local -a deps; deps=($(port-emcee-assert-dependencies "port" "sudo"))
  local admin; admin="${deps[2]}"
  local macports; macports="${deps[1]}"
  # -N non-interactive
  # -p proceed; despite errors
  # -s source-only mode
  # -u uninstall non-active ports
  # -v verbose mode
  local rc
  eval "${admin} ${macports} -Npsuv upgrade outdated"
  rc=${?}
  if [ ${rc} -eq 0 ]
  then
    PORT_EMCEE_INSTALL=""
    export PORT_EMCEE_INSTALL
    PORT_EMCEE_UNINSTALL=""
    export PORT_EMCEE_UNINSTALL
    PORT_EMCEE_UPDATES=""
    export PORT_EMCEE_UPDATES
    echo "cache cleared"
    return ${rc}
  fi
  return ${rc}
}
