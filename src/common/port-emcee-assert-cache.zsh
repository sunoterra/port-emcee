##
## port-emcee-assert-cache
## cache assertions common among all emcee commands
##
function port-emcee-assert-cache() {
  local day; day=86400
  local diff; diff=
  local now; now=$(date "+%s")
  if [[ -z "${PORT_EMCEE_INSTALL}" ]]
  then
    PORT_EMCEE_INSTALL=""
    export PORT_EMCEE_INSTALL
  fi
  if [[ -z "${PORT_EMCEE_UNINSTALL}" ]]
  then
    PORT_EMCEE_UNINSTALL=""
    export PORT_EMCEE_UNINSTALL
  fi
  if [[ -z "${PORT_EMCEE_UPDATES}" ]]
  then
    PORT_EMCEE_UPDATES=""
    export PORT_EMCEE_UPDATES
  fi
  if [[ -z "${PORT_EMCEE_UPDATE_COMPLETE}" ]]
  then
    PORT_EMCEE_UPDATE_COMPLETE=0
    export PORT_EMCEE_UPDATE_COMPLETE
  fi
  if [[ -z "${PORT_EMCEE_UPDATE_TIME}" ]]
  then
    PORT_EMCEE_UPDATE_TIME=${now}
    export PORT_EMCEE_UPDATE_TIME
  fi
  if [[ ${PORT_EMCEE_UPDATE_TIME} -ne ${now} ]]
  then
    diff=$(((now - PORT_EMCEE_UPDATE_TIME)))
    if [[ ${diff} -gt ${day} ]]
    then
      PORT_EMCEE_INSTALL=""
      PORT_EMCEE_UNINSTALL=""
      PORT_EMCEE_UPDATES=""
      PORT_EMCEE_UPDATE_COMPLETE=0
    fi
  fi
}
