##
## port-emcee-assert-dependencies
## external binary dependency assertions common among all emcee commands
##
function port-emcee-assert-dependencies() {
  local -a deps; deps=()
  local admin; admin=$(whence -p sudo)
  local gawk; gawk="/opt/local/bin/gawk"
  local grep; grep="/opt/local/bin/ggrep"
  local gsort; gsort="/opt/local/bin/gsort"
  local gstat; gstat="/opt/local/bin/gstat"
  local macports; macports="/opt/local/bin/port"
  local trimmer; trimmer="/opt/local/bin/port_cutleaves"
  while [ "${#}" -gt 0 ]
  do
    case "${1}"
    in
      "gawk")
        if [ ! -x "${gawk}" ]
        then
          gawk="$(whence -p awk)"
        fi
        if [ ! -x "${gawk}" ]
        then
          port-emcee-messages-dependencies 1
          exit 1
        fi
        deps+=(${gawk})
      ;;
      "grep")
        if [ ! -x "${grep}" ]
        then
          grep="$(whence -p grep)"
          exit 1
        fi
        if [ ! -x "${grep}" ]
        then
          port-emcee-messages-dependencies 2
          exit 1
        fi
        deps+=(${grep})
      ;;
      "gsort")
        if [ ! -x "${gsort}" ]
        then
          gsort="$(whence -p sort)"
        fi
        if [ ! -x "${gsort}" ]
        then
          port-emcee-messages-dependencies 3
          exit 1
        fi
        deps+=(${gsort})
      ;;
      "gstat")
        if [ ! -x "${gstat}" ]
        then
          gstat="$(whence -p stat)"
          exit 1
        fi
        if [ ! -x "${gstat}" ]
        then
          port-emcee-messages-dependencies 4
          exit 1
        fi
        deps+=(${gstat})
      ;;
      "port")
        if [ ! -x "${macports}" ]
        then
          port-emcee-messages-dependencies 0
          exit 1
        fi
        deps+=(${macports})
      ;;
      "port_cutleaves")
        if [ ! -x "${trimmer}" ]
        then
          port-emcee-messages-dependencies 5
          exit 1
        fi
        deps+=(${trimmer})
      ;;
      "sudo")
        if [ ! -x "${admin}" ]
        then
          port-emcee-messages-dependencies 6
          exit 1
        fi
        deps+=(${admin})
      ;;
    esac
    shift
  done
  echo "${deps[*]}"
}
