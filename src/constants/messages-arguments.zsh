##
## port-emcee-messages-arguments
## provide invalid/missing argument constants in a form that does not dirty up the environment
##
## arguments are positive integers 0..n-1, valid arguments are:
## 0 - invalid/missing port-name
##
function port-emcee-messages-arguments() {
  local message=""
  case ${1}
  in
    0)
      message="Please specify a valid port name"
    ;;
  esac
  echo "${message}"
}
