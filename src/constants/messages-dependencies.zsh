##
## port-emcee-messages-dependencies
## provide missing dependency constants in a form that does not dirty up the environment
##
## arguments are positive integers 0..n-1, valid arguments are:
## 0 - missing macports
## 1 - missing (g)awk
## 2 - missing grep
## 3 - missing (g)sort
## 4 - missing (g)stat
## 5 - missing port_cutleaves
## 6 - missing sudo
function port-emcee-messages-dependencies() {
  local message=""
  case ${1}
  in
    0)
      message="Please install macports <https://www.macports.org/>"
    ;;
    1)
      message="Please install gawk e.g. \"$ port-install gawk\""
    ;;
    2)
      message="Please install grep e.g. \"$ port-install grep\""
    ;;
    3)
      message="Please install gsort e.g. \"$ port-install coreutils\""
    ;;
    4)
      message="Please install gstat e.g. \"$ port-install coreutils\""
    ;;
    5)
      message="Please install port_cutleaves e.g. \"$ port-install port_cutleaves\""
    ;;
    6)
      message="macports requires sudo privileges"
    ;;
  esac
  echo "${message}"
}
