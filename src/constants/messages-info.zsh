##
## port-emcee-messages-info
## provide info constants in a form that does not dirty up the environment
##
## arguments are positive integers 0..n-1, valid arguments are:
## 0 - install/uninstall header
function port-emcee-messages-info() {
  local message=""
  case ${1}
  in
    0)
      message=" port                     @version[+variant1..+variantN] \
      \n--------------------------------------------------------------------------------"
    ;;
  esac
  echo "${message}"
}
