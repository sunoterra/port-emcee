####
#### file:        port-emcee.zsh
#### description: zsh plugin - port-emcee
####              one-word commands for various macports operations:
####              port-udpate, port-upgrade, port-outdated, port-trim
#### references:
####
#### author:      Michael Stilson <michael.stilson@me.com>
#### version:     {{port-emcee_VERSION}}
####

